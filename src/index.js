import 'bootstrap' // importeer bootstrap JavaScript code
// import * as bootstrap from 'bootstrap'

import {Popover} from "bootstrap";

import 'bootstrap/dist/css/bootstrap.css' // importeer bootstrap CSS code
import './css/style.css'


const popoverElements = document.querySelectorAll("[data-bs-toggle=\"popover\"]")
console.log(popoverElements)

for (let element of popoverElements) {
    new Popover(element);
}
